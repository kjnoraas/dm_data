import pandas as pd
import numpy as np
import datetime as dt

df = pd.read_csv('master_dataset_2019-4-1_rundays_1_test.txt')
df.set_index('date', drop=True, inplace=True) # key=col name

# to iterate over all columns
col_num=0
condition = df.iloc[:, col_num].isnull()
# index = np.where(condition)[0]     # returns original index
nan_index_list = df.loc[condition].index.to_list()    # returns the datetime as index ina a list
print('-> found %s missing dt with NaN:\n%s' % (len(nan_index_list), nan_index_list))
# will fill this list later from a WS function
ws_diurnal_cycle = [i*1000 for i in range(0,24,1)]

# we pick each dt with nan and find the estimated value for it
for element in nan_index_list:
    # change str to dt
    dt_obj = dt.datetime.strptime(element, '%Y-%m-%d %H:%M:%S')
    # only extract the hr from dt
    hr_of_day = dt_obj.hour
    # find the suitable value from the estimated list
    ws_estimated = ws_diurnal_cycle[hr_of_day]
    # update NaN with estimated value
    df.loc[element, 'E_Sagebrush'] = ws_estimated

print(df.head(20))